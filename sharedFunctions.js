const AWS = require('aws-sdk');
const SNS = new AWS.SNS();


//send message to SNS
exports.sendSns = async function(topic, message) {
   return new Promise(
      function(resolve, reject) {
         var params = {
            Message: message,
            TopicArn: topic
         };

         SNS.publish(params, function(err, data) {
            if (err) {
               console.log('SNS error: %s', err);
               reject(err);
            }
            else {
               resolve(data);
            }
         });
      }
   );
};


//send message to SNS
exports.sendSnsContact = async function(contact) {
   return new Promise(
      function(resolve, reject) {
         var params = {
            Message: JSON.stringify(contact),
            TopicArn: process.env.SNS_TAG_CONTACTS,
            MessageAttributes: {
               gatewayId: {
                  DataType: 'String',
                  StringValue: contact.gateway_id
               },
               tag_a: {
                  DataType: 'String',
                  StringValue: contact.tag_a
               },
               tag_b: {
                  DataType: 'String',
                  StringValue: contact.tag_b
               },
               range: {
                  DataType: 'Number',
                  StringValue: contact.range.toString()
               }
            }
         };

         if (contact.meta.hasOwnProperty('user_a')) {
            params.MessageAttributes.user_a = {
               DataType: 'Number',
               StringValue: contact.meta.user_a.user_id
            };
            params.MessageAttributes.user_a_type = {
               DataType: 'String',
               StringValue: contact.meta.user_a.user_type
            };
         }

         if (contact.meta.hasOwnProperty('user_b')) {
            params.MessageAttributes.user_b = {
               DataType: 'Number',
               StringValue: contact.meta.user_b.user_id
            };
            params.MessageAttributes.user_b_type = {
               DataType: 'String',
               StringValue: contact.meta.user_b.user_type
            };
         }


         SNS.publish(params, function(err, data) {
            if (err) {
               console.log('SNS error: %s', err);
               reject(err);
            }
            else {
               resolve(data);
            }
         });
      }
   );
};
