'use strict';
const SF = require('../../sharedFunctions.js');
const POM = require('../../sharedPostgres.js');

exports.processData = async(event) => {

    //add account metadata
    if (process.env.ADD_POM_META == 'true') {
        event.meta = {
            gateway: await POM.getGateway(event.gatewayId)
        };
    }
    //send to SQS
    await SF.sendSns(process.env.SNS_GATEWAY_STATUS, JSON.stringify(event));

    return event;
};

/*
{
  "messageType": "gatewayStatus",
  "gatewayId": "354616090324281",
  "eventTime": "2020-11-19T19:23:20.000Z",
  "firmware": 179495,
  "rssi": -88,
  "sinr": 23
}
*/
