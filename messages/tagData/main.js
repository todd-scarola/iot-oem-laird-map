'use strict';
const SF = require('../../sharedFunctions.js');
const POM = require('../../sharedPostgres.js');

//device account metadata info
var meta = {};

exports.processData = async(event) => {

   var results = [];

   //add gateway and tag metadata
   if (process.env.ADD_POM_META == 'true') {
      meta = {
         gateway: await POM.getGateway(event.gateway_id),
         tag_a: await POM.getTag(event.deviceId)
      };
   }

   var uploadTime = event.lastUploadTime;

   for (let i = 0; i < event.entries.length; i++) {
      var entry = event.entries[i];

      //trap for 0 on timestamps --bad tags
      if (entry.timestamp > 0 && event.deviceId != entry.serial) {
         var contact = null;

         //add tag metadata
         if (process.env.ADD_POM_META == 'true') {
            meta.tag_b = await POM.getTag(entry.serial);
         }

         //loop through scans and aggregate consecutive scans
         for (let j = 0; j < entry.logs.length; j++) {
            var scan = entry.logs[j];
            if (scan.recordType == 17) {

               //build proposed contact, clone to ensure object independence
               var proposed_contact = JSON.parse(JSON.stringify(await this.createContact(event, entry, scan)));

               //first time through, seed the contact with 1st entry
               if (!contact) { contact = JSON.parse(JSON.stringify(proposed_contact)); }

               //for use by tag status update at the end
               uploadTime = proposed_contact.event_end;

               //extend contacts only
               if (proposed_contact.event_start >= contact.event_end) {

                  //interpolate to determine if we extend this exposure or create a new one
                  if ((Number(proposed_contact.event_start) > (Number(contact.event_end) + Number(process.env.INTERPOLATE))) || (Number(contact.range) != Number(proposed_contact.range))) {

                     //send to SQS
                     await SF.sendSnsContact(contact);

                     results.push(contact);

                     //clone proposed contact to start accruing new one
                     contact = JSON.parse(JSON.stringify(proposed_contact));

                  }
                  else {

                     //mod end date
                     if (proposed_contact.event_start >= contact.event_end) {

                        //extend end date
                        contact.event_end = proposed_contact.event_end;

                        //accrue time in this distance range
                        contact.duration += proposed_contact.duration;
                     }
                  }
               }
            }
         }
         //save any in-progress contact
         if (contact) {

            //send to SNS
            await SF.sendSnsContact(contact);

            results.push(contact);
         }
      }
   }


   //add a tag status
   var tag_status = {
      messageType: 'tagStatus',
      gateway_id: event.gatewayId,
      tag_id: event.deviceId,
      battery_level: Number(event.batteryLevel),
      battery_percent: Number(event.batteryPercent),
      firmware: event.fwVersion,
      device_time: Number(event.deviceTime),
      upload_time: Number(uploadTime),
      previous_upload_time: event.lastUploadTime,
      received_time: Number(new Date().getTime())
   };

   //add account metadata
   tag_status.meta = JSON.parse(JSON.stringify(meta));

   //send to SQS
   await SF.sendSns(process.env.SNS_TAG_STATUS, JSON.stringify(tag_status));

   return results;

};


exports.createContact = async(event, entry, scan) => {

   var contact = {
      messageType: 'tagContact',
      gateway_id: event.gatewayId,
      received_at_gateway: event.deviceTime,
      received_at_aws: event.receivedAt,
      tag_a: event.deviceId,
      tag_b: entry.serial,
      event_start: Number(Number(entry.timestamp) + (Number(scan.log.delta) * Number(entry.scanInterval))),
      event_end: (Number(Number(entry.timestamp) + (Number(scan.log.delta) * Number(entry.scanInterval))) + Number(entry.scanInterval)),
      range: Number(scan.log.range),
      duration: Number(entry.scanInterval)
   };

   //add account metadata
   contact.meta = JSON.parse(JSON.stringify(meta));

   if (process.env.ADD_POM_META == 'true') {
      contact.meta.user_a = await POM.getUser(contact.tag_a, contact.event_start);
      contact.meta.user_b = await POM.getUser(contact.tag_b, contact.event_start);
   }

   //debug info for BL development
   /*
   var debug = {
      log: scan, //log entry used to begin this contact
      interpolate_value: process.env.INTERPOLATE,
      event: event, //full raw event
   };
   contact.debug = debug;
   */

   return contact;
};

/*
{
  "entryProtocolVersion": 2,
  "deviceId": "fa0ca0ec8ba7",
  "deviceTime": 1605799882,
  "lastUploadTime": 1605795629,
  "fwVersion": "00020800",
  "batteryLevel": 2944,
  "networkId": 65535,
  "entries": [
    {
      "entryStart": 165,
      "flags": 253,
      "scanInterval": 30,
      "serial": "e9ed73c7708a",
      "timestamp": 1605795682,
      "length": 256,
      "logs": [
        {
          "recordType": 17,
          "log": {
            "delta": 0,
            "rssi": -38,
            "motion": 0,
            "txPower": 0
          }
        },
        {
          "recordType": 17,
          "log": {
            "delta": 32,
            "rssi": -37,
            "motion": 0,
            "txPower": 0
          }
        }
      ]
    }
  ],
  "gatewayId": "354616090321758",
  "messageType": "tagData"
}
*/
