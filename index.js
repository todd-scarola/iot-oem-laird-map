const SF = require('./sharedFunctions.js');

//inboud message is an SQS trigger
exports.handler = async(event) => {
    
    //console.log('EVENT: %j', event);
    var responses = [];

    //need to figure out which property to ID SQS messages
    if (event.hasOwnProperty('Records'))
    //SQS event
    {
        for (const record of event.Records) {
            responses.push(await this.processMessage(JSON.parse(record.body)));
        }
        return responses;
    }
    else
    //normal JSON request
    {
        return await this.processMessage(event);
    }
};


exports.processMessage = async(event) => {
    try {
        //get processing function
        var helper = null;
        if (event.hasOwnProperty('messageType')) {

            helper = require('./messages/' + event.messageType + '/main.js');
        }

        if (helper) {
            var message = await helper.processData(event);
            if (message) {
                return message;
            }
        }
    }
    catch (error) {
        console.log(error);
    }
};
