const { Client } = require('pg');
//see this for connection string: https://node-postgres.com/features/connecting


exports.executeQuery = async(sql, params) => {
    //console.log('sql: %s params: %s', sql, params);

    //db Client
    var client = new Client();

    try {
        await client.connect();
        var result = await client.query(sql, params);
        //console.log('sql result: %s', result);
    }
    catch (error) {
        if (error.code == 23505) {
            console.log('Duplicate Key: %s, %s', sql, params);
        }
        else {
            console.log(error);
        }
    }

    //cleanup connection
    await client.end();

    return result;
};


//get the currently open contact between 2 tags
exports.getUserByTag = async(tagId, eventTime) => {
    var sql = "SELECT * FROM registrations WHERE \
    tag_id=$1 AND ( \
      (event_start <= $3 AND event_end >= $3) OR \
      (event_start >= $3 AND event_end <= $4) OR \
      (event_start <= $4 AND event_end >= $4) \
      ) ORDER BY event_start ASC LIMIT 1";

    var params = [
        tagId,
        eventTime
    ];

    var result = await this.executeQuery(sql, params);
    if (result.hasOwnProperty('rowCount')) {
        if (result.rowCount > 0) { return result.rows[0]; }
    }
};



exports.getTag = async(tagId) => {
    var sql = "SELECT id, mac_address, account_id, manufacturer, model, nickname, box_number FROM tags WHERE mac_address = $1 LIMIT 1";

    var params = [
        tagId
    ];

    var result = await this.executeQuery(sql, params);
    if (result.hasOwnProperty('rowCount')) {
        if (result.rowCount > 0) { return result.rows[0]; }
    }
};



exports.getGateway = async(gatewayId) => {
    var sql = "SELECT id, imei_number, account_id, location FROM gateways WHERE imei_number = $1 LIMIT 1";

    var params = [
        gatewayId
    ];

    var result = await this.executeQuery(sql, params);
    if (result.hasOwnProperty('rowCount')) {
        if (result.rowCount > 0) { return result.rows[0]; }
    }
};


exports.getUser = async(tagId, eventTime) => {
    var sql = "SELECT r.registerable_id AS user_id, \
       r.registerable_type AS user_type, \
       r.created_at AS checked_in_at, \
       r.checked_out_at \
       FROM registrations r JOIN tags t ON t.id = r.tag_id \
       WHERE t.mac_address = $1 AND r.created_at <= $2 AND (r.checked_out_at >= $2 OR r.checked_out_at IS NULL) LIMIT 1";

    var params = [
        tagId,
        new Date(Number(eventTime) * 1000), //in seconds
    ];

    var result = await this.executeQuery(sql, params);
    if (result.hasOwnProperty('rowCount')) {
        if (result.rowCount > 0) { return result.rows[0]; }
    }
};
